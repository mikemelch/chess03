package tests;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import chess.AttackRoutes;
import chess.Board;
import chess.Chess;
import chess.Chess.InvalidMoveException;
import chess.Path;
import chess.Square;
import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

public class Test {

    @SuppressWarnings("resource")
    public static void playGame(String filePath) {
        BufferedReader br = null;
        String line = null;
        try {
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            System.err.println("File does not exist!");
            return;
        }
        Chess.startGame();

        try {
            while((line = br.readLine()) != null) {
                try {
                    Chess.processInput(line);
                    Chess.changeTurns();
                } catch (InvalidMoveException e) {
                    e.printStackTrace();
                    System.out.println("Invalid Move " + line);
                    Board.showBoard();
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Board.showBoard();
        Board.printBoard();
    }

    public static void setFlagsOfAttackRoutes(AttackRoutes ar) {
        for(Path path : ar.getPaths()) {
            for(Square square : path) {
                square.setFlag(true);
            }
        }
    }

    public static void testPawn() {
        Board.initCustomBoard();
        Pawn p = new Pawn(Piece.Color.WHITE);
        Board.placePiece("b2", p);
        AttackRoutes ar  = p.generatePossibleMoves(Board.getSquareFromChessString("b2"));
        setFlagsOfAttackRoutes(ar);
        Board.printBoard();
        System.out.println(ar);

        Pawn p1 = new Pawn(Piece.Color.BLACK);
        Board.placePiece("c3", p1);
        ar  = p.generatePossibleMoves(Board.getSquareFromChessString("b2"));
        setFlagsOfAttackRoutes(ar);
        Board.printBoard();
        System.out.println(ar);
    }

    public static void testQueen() {
        Board.initCustomBoard();
        Queen p = new Queen(Piece.Color.BLACK);
        Board.placePiece("e5", p);
        Board.placePiece("h5", new Pawn(Piece.Color.BLACK));
        Board.placePiece("e6", new Pawn(Piece.Color.WHITE));
        AttackRoutes ar  = p.generatePossibleMoves(Board.getSquareFromChessString("e5"));
        setFlagsOfAttackRoutes(ar);
        Board.printBoard();
        System.out.println(ar);
    }

    public static void testKing() {
        Board.initCustomBoard();
        King p = new King(Piece.Color.BLACK);
        Board.placePiece("e5", p);
        AttackRoutes ar  = p.generatePossibleMoves(Board.getSquareFromChessString("e5"));
        setFlagsOfAttackRoutes(ar);
        Board.printBoard();
        System.out.println(ar);
    }

    public static void testRook() {
        Board.initCustomBoard();
        Rook p = new Rook(Piece.Color.BLACK);
        Board.placePiece("e5", p);
        AttackRoutes ar  = p.generatePossibleMoves(Board.getSquareFromChessString("e5"));
        setFlagsOfAttackRoutes(ar);
        Board.printBoard();
        System.out.println(ar);
    }

    public static void testBishop() {
        Board.initCustomBoard();
        Bishop p = new Bishop(Piece.Color.BLACK);
        Board.placePiece("e5", p);
        AttackRoutes ar  = p.generatePossibleMoves(Board.getSquareFromChessString("e5"));
        setFlagsOfAttackRoutes(ar);
        Board.printBoard();
        System.out.println(ar);
    }

    public static void testKnight() {
        Board.initCustomBoard();
        Knight p = new Knight(Piece.Color.BLACK);
        Board.placePiece("e5", p);
        Board.placePiece("f7", new Pawn(Piece.Color.WHITE));
        AttackRoutes ar  = p.generatePossibleMoves(Board.getSquareFromChessString("e5"));
        setFlagsOfAttackRoutes(ar);
        Board.printBoard();
        System.out.println(ar);
    }

    public static void initialBoard() {
        Board.populateBoard();
        String files = "abcdefgh";
        String ranks = "1278";

        for(char f : files.toCharArray()) {
            for(char r : ranks.toCharArray()) {
                System.out.println(f + "" + r);
                Square sq = Board.getSquareFromChessString(f + "" + r);
                Piece p = sq.getOccupant();
                AttackRoutes attackRoutes = p.generatePossibleMoves(sq);
                setFlagsOfAttackRoutes(attackRoutes);
                System.out.println(attackRoutes);
            }
        }
        Board.showBoard();
    }

    public static void castleCheck() {
        Board.initCustomBoard();
        King p1 = new King(Piece.Color.WHITE);
        Rook p2 = new Rook(Piece.Color.WHITE);
        Rook p3 = new Rook(Piece.Color.WHITE);

        Board.placePiece("e1", p1);
        Board.placePiece("a1", p2);
        Board.placePiece("h1", p3);
        AttackRoutes ar  = p1.generatePossibleMoves(Board.getSquareFromChessString("e1"));
        setFlagsOfAttackRoutes(ar);

        ar = p2.generatePossibleMoves(Board.getSquareFromChessString("a1"));
        setFlagsOfAttackRoutes(ar);

        ar = p3.generatePossibleMoves(Board.getSquareFromChessString("h1"));
        setFlagsOfAttackRoutes(ar);

        Board.showBoard();
    }

    // This is a checkmate
    private static void checkMateTest1() {
        Board.initCustomBoard();
        Board.placePiece("a1", new King(Piece.Color.WHITE));
        Board.placePiece("b5", new Rook(Piece.Color.BLACK));
        Board.placePiece("b2", new Queen(Piece.Color.BLACK));

        Board.showBoard();

        System.out.println(Board.isInCheckMate(Piece.Color.WHITE));
    }

    // This isn't a checkmate
    private static void checkMateTest2() {
        Board.initCustomBoard();
        Board.placePiece("a1", new King(Piece.Color.WHITE));
        Board.placePiece("b5", new Rook(Piece.Color.BLACK));
        Board.placePiece("b2", new Queen(Piece.Color.BLACK));
        Board.placePiece("h8", new Bishop(Piece.Color.WHITE));

        Board.showBoard();

        System.out.println(Board.isInCheckMate(Piece.Color.WHITE));
    }

    // This is a checkmate
    private static void checkMateTest3() {
        Board.initCustomBoard();
        Board.placePiece("a1", new Rook(Piece.Color.BLACK));
        Board.placePiece("b5", new Rook(Piece.Color.BLACK));
        Board.placePiece("a7", new King(Piece.Color.WHITE));
        //Board.placePiece("h8", new Bishop(Piece.Color.WHITE));

        Board.showBoard();

        System.out.println(Board.isInCheckMate(Piece.Color.WHITE));
    }

    // This is a checkmate
    private static void checkMateTest4() {
        Board.initCustomBoard();
        Board.placePiece("a2", new Pawn(Piece.Color.WHITE));
        Board.placePiece("b2", new Pawn(Piece.Color.WHITE));
        Board.placePiece("c2", new Pawn(Piece.Color.WHITE));
        Board.placePiece("d2", new Pawn(Piece.Color.WHITE));
        Board.placePiece("e2", new Pawn(Piece.Color.WHITE));
        Board.placePiece("a1", new Rook(Piece.Color.WHITE));
        Board.placePiece("e1", new King(Piece.Color.WHITE));
        Board.placePiece("f2", new Pawn(Piece.Color.WHITE));
        Board.placePiece("h1", new Queen(Piece.Color.BLACK));

        Board.showBoard();

        System.out.println(Board.isInCheckMate(Piece.Color.WHITE));
    }

    // This is a stalemate
    private static void stalemateTest1() {
        Board.initCustomBoard();
        //Board.placePiece("g6", new King(Piece.Color.WHITE));
        Board.placePiece("f7", new Queen(Piece.Color.WHITE));
        Board.placePiece("h8", new King(Piece.Color.BLACK));

        Board.showBoard();

        System.out.println(Board.isInStaleMate(Piece.Color.BLACK));
    }

    // This is a stalemate
    private static void stalemateTest2() {
        Board.initCustomBoard();
        Board.placePiece("f6", new King(Piece.Color.WHITE));
        Board.placePiece("f7", new Pawn(Piece.Color.WHITE));
        Board.placePiece("f8", new King(Piece.Color.BLACK));

        Board.showBoard();

        System.out.println(Board.isInStaleMate(Piece.Color.BLACK));
    }

    // This is a stalemate
    private static void stalemateTest3() {
        Board.initCustomBoard();
        Board.placePiece("b6", new King(Piece.Color.WHITE));
        Board.placePiece("h8", new Rook(Piece.Color.WHITE));
        Board.placePiece("a8", new King(Piece.Color.BLACK));
        Board.placePiece("b8", new Bishop(Piece.Color.BLACK));

        Board.showBoard();

        System.out.println(Board.isInStaleMate(Piece.Color.BLACK));
    }

    // This is a stalemate
    private static void stalemateTest4() {
        Board.initCustomBoard();
        Board.placePiece("c3", new King(Piece.Color.WHITE));
        Board.placePiece("b2", new Rook(Piece.Color.WHITE));
        Board.placePiece("a1", new King(Piece.Color.BLACK));

        Board.showBoard();

        System.out.println(Board.isInStaleMate(Piece.Color.BLACK));
    }

    public static void main(String args[]) throws IOException, InvalidMoveException {
        playGame("resources/ByrneFischer1956");
        //stalemateTest3();
        //initialBoard();
    }
}
