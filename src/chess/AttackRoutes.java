 package chess;

import java.util.ArrayList;
import java.util.List;

/**
 * AttackRoutes is a structure for storing all possible Paths for a Piece.
 * Any time that moves are generated for pieces, the resulting object
 * is an AttackRoutes object. This class also contains important methods
 * for testing whether or not a move is valid, and getting all Paths
 * that result in "Check."
 *
 * @author Mike Melchione
 * @author Aaron Rosenheck
 */
public class AttackRoutes {

    /**
     * All paths that are possible for a particular piece
     */
    private List<Path> paths;


    /**
     * Creates a new AttackRoutes object
     */
    public AttackRoutes() {
        paths = new ArrayList<Path>();
    }


    /**
     * Merges two AttackRoutes into one. The parameters are unchanged.
     *
     * @param ar1   AttackRoutes containing Paths to be added to output
     * @param ar2   AttackRoutes containing Paths to be added to output
     * @return      AttackRoutes object which contains all Paths of ar1 and ar2
     */
    public static AttackRoutes merge(AttackRoutes ar1, AttackRoutes ar2) {
        ar1.paths.addAll(ar2.paths);
        return ar1;
    }


    /**
     * Retrieves all Paths that result in "Check" for a given King.
     *
     * @param routes        possible AttackRoutes for the "attacking" player
     * @param kingLocation  king location for the "defending" player
     * @return              newly constructed AttackRoutes object which contains
     *                      all "Check" routes
     *
     * @see Square
     * @see Path
     */
    public static AttackRoutes getCheckRoutes(List<AttackRoutes> routes, Square kingLocation) {
        AttackRoutes checkRoutes = new AttackRoutes();
        for(AttackRoutes ar : routes) {
            for(Path path : ar.getPaths()) {
                if(path.contains(kingLocation)) {
                    checkRoutes.addPath(path);
                }
            }
        }
        return checkRoutes;
    }


    /**
     * Tests all Paths within an AttackRoutes structure to see if a given
     * Square is contained in any Path.
     *
     * @param sq    the Square to check for
     * @return      <code>true</code> if it exists in any Path
     *              <br>
     *              <code>false</code> otherwise
     *
     * @see Square
     * @see Path
     */
    public boolean containsPossibleSquare(Square sq) {
        for(Path path : paths) {
            if(path.contains(sq)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Retrieves the Path which ends with the given Square.
     *
     * @param sq    the Square to check for
     * @return      the Path that ends with sq
     *              <br>
     *              <code>null</code> if this Path does not exist
     *
     * @see Square
     * @see Path
     */
    public Path getPathWithEndSquare(Square sq) {
        for(Path path : paths) {
            if(path.contains(sq)) {
                return path;
            }
        }
        return null;
    }


    /**
     * {@link AttackRoutes#paths}
     */
    public List<Path> getPaths() {
        return this.paths;
    }


    /**
     * {@link AttackRoutes#paths}
     */
    public void addPath(Path path) {
        paths.add(path.copy());
    }


    @Override
    public String toString() {
        return "AttackRoutes [paths=" + paths + "]";
    }
}
