package chess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import pieces.Piece;

public class Chess {

    /**
     * Constants for output messages
     */
    private final static String OM_CHECK = "Check";
    private final static String OM_CHECKMATE = "Checkmate";
    private final static String OM_DRAW = "Draw";
    private final static String OM_INVALID_MOVE = "Illegal move, try again";
    private final static String OM_STALEMATE = "Stalemate";

    /**
     * Reader for taking user input from command line
     */
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Keeps track of whether a draw has been proposed
     */
    private static boolean drawProposed = false;

    /**
     * Keeps track of whose turn it is. White always moves first.
     */
    private static Piece.Color currentTurn = Piece.Color.WHITE;

    /**
     * Structure which is used to tell the current game state.
     *
     * @author Michael Melchione
     */
    private enum GameCode {PLAY, STOP};

    /**
     * Invalid move exception with a default message.
     *
     * @author Michael Melchione
     *
     * @see Exception
     */
    @SuppressWarnings("serial")
    public static class InvalidMoveException extends Exception {
        public InvalidMoveException(String message) {
            super(message);
        }
        public InvalidMoveException() {
            this(OM_INVALID_MOVE);
        }
    }


    /**
     * The first step processing for a user's move.
     *
     * @param startString               String representation of the start Square from user
     * @param endString                 String representation of the end Square from user
     * @return                          GameCode which says the play state
     * @throws InvalidMoveException     if the move is invalid
     *
     * @see Chess.GameCode
     */
    private static GameCode userMove(String startString, String endString) throws InvalidMoveException {
        if(startString.equals(endString)) {
            throw new InvalidMoveException();
        }
        Square startSquare = Board.getSquareFromChessString(startString);
        Square endSquare;
        AttackRoutes possibleRoutes;

        if(startSquare == null || startSquare.isEmpty() || startSquare.getOccupant().getColor() != currentTurn) {
            throw new InvalidMoveException();
        }

        endSquare = Board.getSquareFromChessString(endString);
        if(endSquare == null) {
            throw new InvalidMoveException();
        }
        possibleRoutes = startSquare.getOccupant().generatePossibleMoves(startSquare);

        Path path = possibleRoutes.getPathWithEndSquare(endSquare);

        if(path != null) {
            Board.movePiece(startSquare, endSquare, path);
            if(Board.isInCheck(currentTurn.flipColor())) {
                if(Board.isInCheckMate(currentTurn.flipColor())) {
                    System.out.println(OM_CHECKMATE);
                    System.out.println(currentTurn + " wins");
                    return GameCode.STOP;
                }
                else {
                    System.out.println(OM_CHECK);
                }
            }
            else if(Board.isInStaleMate(currentTurn.flipColor())) {
                System.out.println(OM_STALEMATE);
                return GameCode.STOP;
            }
        } else {
            throw new InvalidMoveException();
        }

        return GameCode.PLAY;
    }


    /**
     * Promotes pawn to a given piece if, and only if, it is provided by the user.
     *
     * @param endString                 location of the pawn
     * @param pieceCode                 Piece the user wishes to promote to
     * @return                          GameCode which says the play state
     * @throws InvalidMoveException     if the move is invalid
     *
     * @see Chess.GameCode
     */
    private static GameCode pawnPromotion(String endString, String pieceCode) throws InvalidMoveException {
        Square square = Board.getSquareFromChessString(endString);
        Piece p = Piece.getPieceFromCode(pieceCode, square.getOccupantColor());

        square.setOccupant(p);

        if(Board.isInCheck(currentTurn.flipColor())) {
            if(Board.isInCheckMate(currentTurn.flipColor())) {
                System.out.println(currentTurn + " wins");
                return GameCode.STOP;
            }
            else {
                System.out.println(OM_CHECK);
            }
        }
        else if(Board.isInStaleMate(currentTurn.flipColor())) {
            System.out.println(OM_STALEMATE);
            return GameCode.STOP;
        }

        return GameCode.PLAY;
    }


    /**
     * Starts a game by populating the game board.
     *
     * @see Board
     */
    public static void startGame() {
        Board.populateBoard();
    }


    /**
     * Processes user input and calls the appropriate methods.
     *
     * @param input                     user input
     * @return                          GameCode which says the play state
     * @throws InvalidMoveException     if the user enters an invalid move
     *
     * @see Chess.GameCode
     */
    public static GameCode processInput(String input) throws InvalidMoveException {
        if(input.equals("resign")) {
            System.out.println("\n" + currentTurn + " resigned.");
            return GameCode.STOP;
        }
        else if(input.equals("draw")) {
            if(drawProposed) {
                System.out.println(OM_DRAW);
                return GameCode.STOP;
            } else {
                throw new InvalidMoveException();
            }
        }
        else {
            String[] inputTokens = input.split(" ");

            if(inputTokens.length == 2) {
                drawProposed = false;
                return userMove(inputTokens[0], inputTokens[1]);
            }
            else if(inputTokens.length == 3) {
                if(inputTokens[2].equals("draw?")) {
                    drawProposed = true;
                    System.out.println("\nDraw propsed.\n");
                    return userMove(inputTokens[0], inputTokens[1]);
                }
                else {
                    drawProposed = false;
                    userMove(inputTokens[0], inputTokens[1]);
                    return pawnPromotion(inputTokens[1], inputTokens[2]);
                }
            }
            else {
                throw new InvalidMoveException();
            }
        }
    }


    /**
     * Controls input flow from the user. Continues until the game ends.
     */
    public static void commandLine() {

        while(true) {
            Board.printBoard();
            String input = null;

            while(input == null) {
                System.out.print(currentTurn + "'s move: ");
                try {
                    input = br.readLine().trim();
                    if(processInput(input) == GameCode.STOP) {
                        return;
                    }
                } catch (IOException e) {
                    System.out.println("Invalid input!");
                    input = null;
                } catch (InvalidMoveException e) {
                    System.out.println(e.getMessage());
                    input = null;
                }
            }

            // Switch turns
            currentTurn = currentTurn.flipColor();
        }
    }


    /**
     * Change from one player's turn to the other.
     */
    public static void changeTurns() {
        currentTurn = currentTurn.flipColor();
    }


    /**
     * Main method entry for Chess. Starts the game and command line.
     *
     * @param args  method arguments - unused
     */
    public static void main(String[] args) {
        startGame();
        commandLine();
    }
}
