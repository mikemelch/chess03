package chess;

import pieces.Piece;

/**
 * Square is a structure which makes up the game board. Each Square represents
 * a location within the game board, and there are always 64 Squares active
 * on a single board.
 *
 * @author Mike Melchione
 * @author Aaron Rosenheck
 */
public class Square {

    /**
     * Constants which define the representation of Squares
     */
    private static final String BLACK_ASCII = "xx";
    private static final String WHITE_ASCII = "  ";
    private static final String FILE_NAMES = "abcdefgh";

    /**
     * Holds the Piece within a Square
     */
    private Piece occupant;

    /**
     * Used for testing purposes
     */
    private boolean flag = false;

    /**
     * Square's row within Board
     */
    final int row;

    /**
     * Square's column within Board
     */
    final int col;


    /**
     * Creates a new Square with a given row and column.
     *
     * @param row   row
     * @param col   column
     */
    public Square(int row, int col) {
        this(row, col, null);
    }


    /**
     * Creates a Square with a given row, column, and Piece.
     *
     * @param row       row
     * @param col       column
     * @param piece     Piece to place
     *
     * @see Piece
     */
    public Square(int row, int col, Piece piece) {
        this.row = row;
        this.col = col;
        this.occupant = piece;
    }


    /**
     * @see Square#row
     * @return  row
     */
    public int getRow() {
        return row;
    }


    /**
     * Retrieves a row with a given offset from the Square instance.
     *
     * @param offset    row offset
     * @return          row with offset
     */
    public int getRowWithOffset(int offset) {
        return row + offset;
    }


    /**
     * Retrieves a column with a given offset from the Square instance.
     *
     * @param offset    column offset
     * @return          column with offset
     */
    public int getColWithOffset(int offset) {
        return col + offset;
    }


    /**
     * @see Square#col
     * @return  column
     */
    public int getCol() {
        return col;
    }


    /**
     * Retrieves the Piece within the Square instance.
     *
     * @return      Piece contained within Square
     *
     * @see Piece
     */
    public Piece getOccupant() {
        return occupant;
    }


    /**
     * Retrieves the Piece's color within the Square instance.
     *
     * @return      Piece.Color within Square.
     *              <br>
     *              {@code null} if there is no Piece within Square.
     *
     * @see Piece.Color
     */
    public Piece.Color getOccupantColor() {
        if(occupant == null) {
            return null;
        }
        return occupant.getColor();
    }


    /**
     * Removes the Piece within the Square.
     */
    public void removeOccupant() {
        this.occupant = null;
    }


    /**
     * Flags the Square instance. Used for testing purposes.
     *
     * @param flag      boolean to set flag to
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }


    /**
     * @see Square#flag
     * @return  flag
     */
    public boolean getFlag() {
        return flag;
    }


    /**
     * Sets the Piece stored within the Square instance.
     *
     * @param occupant  Piece to set
     *
     * @see Piece
     */
    public void setOccupant(Piece occupant) {
        this.occupant = occupant;
    }


    /**
     * Tests to see if current Square instance is empty.
     *
     * @return  {@code true} if Square is empty.
     *          <br>
     *          {@code false} otherwise.
     */
    public boolean isEmpty() {
        if(occupant == null) {
            return true;
        }
        return false;
    }


    /**
     * Retrieves the String representation of the column.
     *
     * @return      String representation of the column
     */
    public String getFile() {
        return String.valueOf(FILE_NAMES.charAt(col));
    }


    /**
     * Retrieves the String representation of the row.
     *
     * @return      String representation of the row
     */
    public String getRank() {
        return String.valueOf(8 - row);
    }


    /**
     * Retrieves the String representation of the Square instance.
     *
     * @return      String representation of the Square instance
     */
    public String getFileRank() {
        return getFile() + getRank();
    }


    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getFileRank();
    }


    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + col;
        result = prime * result + row;
        return result;
    }


    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Square other = (Square) obj;
        if (col != other.col)
            return false;
        if (row != other.row)
            return false;
        return true;
    }


    /**
     * String representation of a Square. This formats the Square for the Board representation.
     * For example, if the Square is empty, we must return the ASCII for a white or black space
     * on the board.
     *
     * @return      String representation of a Square for game board
     */
    public String getString() {
        String result;
        if(occupant != null) {
            result = occupant.toString();
        }
        else { //blank square
            if((row + col) % 2 == 0) {
                result = WHITE_ASCII;
            }
            else {
                result = BLACK_ASCII;
            }
        }
        return result;
    }
}