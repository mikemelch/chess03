package chess;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import chess.Chess.InvalidMoveException;
import chess.Path.PathType;
import pieces.*;

/**
 * Board is a class which holds the actual structure of the game board. It is also
 * responsible for generating moves for the pieces, moving pieces, testing for
 * "Check", "Checkmate", and "Stalemate", and printing the game board.
 *
 * @author Mike Melchione
 * @author Aaron Rosenheck
 *
 * @see Square
 * @see Piece.Color
 */
public class Board {

    /**
     * 2D array of Square objects that represent the game board
     */
    private static Square[][] board = new Square[8][8];

    /**
     * 2D array of Square objects that represent the last committed game board.
     * This is used for testing if a move is valid.
     */
    private static Square[][] checkpointBoard = new Square[8][8];

    /**
     * Holds the current location of the black king
     */
    private static Square blackKingSpot;

    /**
     * Holds the current location of the white king
     */
    private static Square whiteKingSpot;

    /**
     * Holds all possible AttackRoutes for all of white's pieces
     */
    private static List<AttackRoutes> whitePossibleMoves = new ArrayList<AttackRoutes>();

    /**
     * Holds all possible AttackRoutes for all of black's pieces
     */
    private static List<AttackRoutes> blackPossibleMoves = new ArrayList<AttackRoutes>();

    /**
     * Holds the location of a possible "En Passant" Square
     * <br>
     * <code>null</code> if no "En Passant" is possible
     */
    public static Square enpassantSquare = null;


    /**
     * Method used for testing.
     * <p>
     * Creates a new game board with no pieces.
     */
    public static void initCustomBoard() {
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                board[i][j] = new Square(i, j);
                checkpointBoard[i][j] = new Square(i ,j);
            }
        }
    }


    /**
     * Retrieves possible "Castle" moves for a given color. Returns "Castle" moves
     * within an AttackRoutes structure, or <code>null</code> if none exist.
     *
     * @param color     team to return "Castle" moves for
     * @return          "Castle" moves within AttackRoutes structure
     *
     * @see Piece.Color
     */
    public static AttackRoutes getCastlesForColor(Piece.Color color) {
        Square kingLocation;
        Square[] rookLocations = new Square[2];
        AttackRoutes castles = new AttackRoutes();

        if(color == Piece.Color.WHITE) {
            kingLocation = whiteKingSpot;
            rookLocations[0] = board[7][0];
            rookLocations[1] = board[7][7];
        } else {
            kingLocation = blackKingSpot;
            rookLocations[0] = board[0][0];
            rookLocations[1] = board[0][7];
        }

        TrackedPiece king = (TrackedPiece)kingLocation.getOccupant();

        if(king.hasMoved()) {
            return null;
        }

        int kingOffset = -2;
        int rookOffset = 1;
        for(Square rookLocation : rookLocations) {
            if(rookLocation.getOccupant() instanceof Rook && rookLocation.getOccupantColor() == color) {
                TrackedPiece rook = (TrackedPiece)rookLocation.getOccupant();
                if(!rook.hasMoved()) {
                    Path castlePath = new Path(PathType.CASTLE);
                    Square kingEndLocation = Board.getSquareWithOffset(kingLocation, 0, kingOffset);
                    Square rookEndLocation = Board.getSquareWithOffset(kingEndLocation, 0, rookOffset);

                    if(kingEndLocation.isEmpty() && rookEndLocation.isEmpty()) {
                        if(kingOffset > 0 || (kingOffset < 0 && Board.getSquareWithOffset(kingEndLocation, 0, -1).isEmpty())) {
                            castlePath.add(kingLocation);
                            castlePath.add(kingEndLocation);
                            castlePath.setSideEffects(rookLocation, rookEndLocation);
                            castles.addPath(castlePath);
                        }
                    }

                }
            }
            kingOffset *= -1;
            rookOffset *= -1;
        }
        return castles;
    }


    /**
     * Generates all possible moves for a given color. These moves are saved within
     * the Board class
     *
     * @param color     team to generate moves for
     *
     * @see Piece.Color
     */
    private static void generatePossibleMovesForColor(Piece.Color color) {
        List<AttackRoutes> possibleMoves;
        if(color == Piece.Color.WHITE) {
            possibleMoves = whitePossibleMoves;
        }
        else {
            possibleMoves = blackPossibleMoves;
        }
        possibleMoves.clear();

        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                if(board[i][j].getOccupantColor() == color) {
                    possibleMoves.add(board[i][j].getOccupant().generatePossibleMoves(board[i][j]));
                }
            }
        }
    }


    /**
     * Places a give Piece into a given location
     *
     * @param location  String format of a Square (ex: "a1")
     * @param p         Piece to place
     *
     * @see Path
     */
    public static void placePiece(String location, Piece p) {
        Square sq = getSquareFromChessString(location);
        sq.setOccupant(p);

        if(p instanceof King) {
            if(p.getColor().equals(Piece.Color.BLACK)) {
                blackKingSpot = sq;
            }
            else {
                whiteKingSpot = sq;
            }
        }
    }


    /**
     * Reverts board back to the last committed state
     */
    public static void revertState() {
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                board[i][j].setOccupant(checkpointBoard[i][j].getOccupant());
                if(checkpointBoard[i][j].getOccupant() instanceof King) {
                    if(checkpointBoard[i][j].getOccupant().getColor() == Piece.Color.BLACK) {
                        blackKingSpot = checkpointBoard[i][j];
                    } else {
                        whiteKingSpot = checkpointBoard[i][j];
                    }
                }
            }
        }
    }


    /**
     * Saves board to a valid state
     */
    public static void commitState() {
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                checkpointBoard[i][j].setOccupant(board[i][j].getOccupant());
                if(board[i][j].getOccupant() instanceof King) {
                    if(board[i][j].getOccupant().getColor() == Piece.Color.BLACK) {
                        blackKingSpot = board[i][j];
                    } else {
                        whiteKingSpot = board[i][j];
                    }
                }
            }
        }
    }


    /**
     * Moves a piece within the game board from the given start Square to the given end Square.
     * <p>
     * This method will capture a Piece on the end Square if one exists there.
     * <p>
     * This method will update a Piece to having been moved if the move is valid. This is done
     * through the TrackedPiece interface.
     *
     * @param startSquare               starting Square of the move
     * @param endSquare                 ending Square of the move
     * @param path                      Path object which contains extra information about the move.
     *                                  (ex: A "Castle" has some side effects which must be performed
     *                                  after the move)
     * @param commits                   boolean which indicates whether this method should commit after
     *                                  a successful move
     * @throws InvalidMoveException     if the move is not valid (ex: moves into "Check")
     *
     * @see Square
     * @see Path
     * @see TrackedPiece
     */
    public static void movePiece(Square startSquare, Square endSquare, Path path, boolean commits)
            throws InvalidMoveException {

        if(commits) commitState();
        boolean firstMove = false;
        TrackedPiece trackedPiece = null;

        Piece movingPiece = startSquare.getOccupant();
        if(movingPiece instanceof TrackedPiece) {
            trackedPiece = (TrackedPiece) movingPiece;
            if(!trackedPiece.hasMoved()) {
                trackedPiece.setMoved(true);
                firstMove = true;
            }
        }

        if(path == null || path.getPathType() == PathType.DEFAULT) {
            Square tempEnpassant = null;
            if(enpassantSquare != null) {
                tempEnpassant = new Square(enpassantSquare.row, enpassantSquare.col);
                enpassantSquare = null;
            }

            movePieceAndUpdateKingLocation(startSquare, endSquare);
            generatePossibleMovesForColor(movingPiece.getColor().flipColor());

            if(isInCheck(movingPiece.getColor())) {
                revertState();
                enpassantSquare = tempEnpassant;
                if(firstMove) {
                    trackedPiece.setMoved(false);
                }
                throw new InvalidMoveException();
            }
            else {
                if(commits) commitState();
                if(!commits) enpassantSquare = tempEnpassant;
            }
        }

        else if(path.getPathType() == PathType.CASTLE) {
            int direction;
            if(endSquare.getCol() < 4) {
                direction = -1; // Queen-side castle
            } else {
                direction = 1; // King-side castle
            }
            Square currentSquare = startSquare;
            Square nextSquare;

            // Test to see if any squares on the way to castle would result in check
            for(int squaresMoved = 1; squaresMoved <= 2; squaresMoved++) {
                nextSquare = Board.getSquareWithOffset(startSquare, 0, direction * squaresMoved);
                movePieceAndUpdateKingLocation(currentSquare, nextSquare);
                generatePossibleMovesForColor(movingPiece.getColor().flipColor());

                if(isInCheck(movingPiece.getColor())) {
                    revertState();
                    trackedPiece.setMoved(false); // Don't have to check because we were trying to castle
                    throw new InvalidMoveException();
                }
                else {
                    currentSquare = nextSquare;
                }
            }

            path.processSideEffects();

            // Test to see if moving the Rook to its castle position results in Check
            if(isInCheck(movingPiece.getColor())) {
                if(commits) revertState();
                trackedPiece.setMoved(false); // Don't have to check because we were trying to castle
                throw new InvalidMoveException();
            }
            else {
                if(commits) commitState();
            }

        }
        else if(path.getPathType() == PathType.PAWN_DOUBLE) {
            if(startSquare.getOccupantColor() == Piece.Color.BLACK) {
                enpassantSquare = Board.getSquareWithOffset(endSquare, -1, 0);
            }
            else {
                enpassantSquare = Board.getSquareWithOffset(endSquare, 1, 0);
            }

            movePieceAndUpdateKingLocation(startSquare, endSquare);
            generatePossibleMovesForColor(movingPiece.getColor().flipColor());

            if(isInCheck(movingPiece.getColor())){
                if(commits) revertState();
                trackedPiece.setMoved(false);
                enpassantSquare = null;
                throw new InvalidMoveException();
            }
            else {
                if(commits) commitState();
            }
        }
        else if(path.getPathType() == PathType.ENPASSANT) {
            movePieceAndUpdateKingLocation(startSquare, endSquare);
            path.processSideEffects();
            generatePossibleMovesForColor(movingPiece.getColor().flipColor());

            if(isInCheck(movingPiece.getColor())){
                if(commits) revertState();
                trackedPiece.setMoved(false);
                enpassantSquare = null; // En passant not possible
                throw new InvalidMoveException();
            }
            else {
                if(commits) commitState();
            }
        }
        else if(path.getPathType() == PathType.PAWN_PROMOTION) {
            movePieceAndUpdateKingLocation(startSquare, endSquare);
            path.processSideEffects();
            generatePossibleMovesForColor(movingPiece.getColor().flipColor());

            if(isInCheck(movingPiece.getColor())) {
                if(commits) revertState();
                trackedPiece.setMoved(false);
                throw new InvalidMoveException();
            }
            else {
                if(commits) commitState();
            }
        }
    }


    /**
     * {@code commits} defaults to {@code true}.
     *
     * @see Board#movePiece(Square, Square, Path, boolean)
     */
    public static void movePiece(Square startSquare, Square endSquare, Path path)
            throws InvalidMoveException {
        movePiece(startSquare, endSquare, path, true);
    }


    /**
     * Moves a piece from a given start Square to a given end Square.
     * <p>
     * Updates the location of the King if the moving Piece was a King.
     *
     * @param startSquare               starting Square of the move
     * @param endSquare                 ending Square of the move
     * @throws InvalidMoveException     if the move is not valid
     *
     * @see Square
     * @see King
     */
    private static void movePieceAndUpdateKingLocation(Square startSquare, Square endSquare)
            throws InvalidMoveException {

        Piece movingPiece = startSquare.getOccupant();
        placePiece(endSquare.toString(), movingPiece);
        startSquare.removeOccupant();

        if(movingPiece instanceof King) {
            if(movingPiece.getColor().equals(Piece.Color.BLACK)) {
                blackKingSpot = endSquare;
            }
            else {
                whiteKingSpot = endSquare;
            }
        }
    }


    /**
     * Initializes the board to contain the starting positions
     * of all the white and black Pieces.
     *
     * @see Piece
     */
    public static void populateBoard() {
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                board[i][j] = new Square(i, j);
                checkpointBoard[i][j] = new Square(i ,j);
            }
        }

        for(int i = 0; i < 8; i ++) {
            board[1][i].setOccupant(new Pawn(Piece.Color.BLACK));
            board[6][i].setOccupant(new Pawn(Piece.Color.WHITE));
        }
        board[0][0].setOccupant(new Rook(Piece.Color.BLACK));
        board[0][1].setOccupant(new Knight(Piece.Color.BLACK));
        board[0][2].setOccupant(new Bishop(Piece.Color.BLACK));
        board[0][3].setOccupant(new Queen(Piece.Color.BLACK));
        board[0][4].setOccupant(new King(Piece.Color.BLACK));
        board[0][5].setOccupant(new Bishop(Piece.Color.BLACK));
        board[0][6].setOccupant(new Knight(Piece.Color.BLACK));
        board[0][7].setOccupant(new Rook(Piece.Color.BLACK));

        board[7][0].setOccupant(new Rook(Piece.Color.WHITE));
        board[7][1].setOccupant(new Knight(Piece.Color.WHITE));
        board[7][2].setOccupant(new Bishop(Piece.Color.WHITE));
        board[7][3].setOccupant(new Queen(Piece.Color.WHITE));
        board[7][4].setOccupant(new King(Piece.Color.WHITE));
        board[7][5].setOccupant(new Bishop(Piece.Color.WHITE));
        board[7][6].setOccupant(new Knight(Piece.Color.WHITE));
        board[7][7].setOccupant(new Rook(Piece.Color.WHITE));

        blackKingSpot = board[0][4];
        whiteKingSpot = board[7][4];

        commitState();
     }

    /**
     * Prints the board to standard output.
     */
    public static void printBoard() {
        System.out.println();
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j ++) {
                System.out.print(board[i][j].getString() + " ");
            }
            System.out.println((8-i) + " ");
        }
        System.out.println(" a  b  c  d  e  f  g  h");
        System.out.println();
    }


    /**
     * Retrieves the Square at a given row and column.
     *
     * @param row   row index
     * @param col   column index
     * @return      Square at the given row and column. {@code null} if it does not exist
     *
     * @see Square
     */
    public static Square getSquare(int row, int col) {
        if(row < 0 || row > 7 || col < 0 || col > 7 ) {
            return null;
        }
        return board[row][col];
    }


    /**
     * Converts a String formatted Square to a real Square object within the game board.
     *
     * @param strSquare     String formatted Square
     * @return              Square which corresponds to the given String
     *
     * @see Square
     */
    public static Square getSquareFromChessString(String strSquare) {
        if(strSquare == null || strSquare.length() != 2) {
            return null;
        }
        strSquare = strSquare.toLowerCase();
        char col = strSquare.charAt(0);
        char row = strSquare.charAt(1);


        if(col < 'a' || col > 'h' || row < '1' || row > '8') {
            return null;
        }
        int boardRow = 8 - (row - '0'); // Convert row to int, then subtract from 8 for board array
        int boardCol = (col - 'a'); // Convert col to int, then subtract one for board array
        return board[boardRow][boardCol];
    }


    /**
     * Retrieves a Square with an offset to another Square.
     *
     * @param s     Square to base offset off of
     * @param rOff  row offset
     * @param cOff  column offset
     * @return      Square which corresponds to the given offsets. {@code null} if it does not exist
     *
     * @see Square
     */
    public static Square getSquareWithOffset(Square s, int rOff, int cOff) {
        if(s == null) {
            return null;
        }
        return getSquare(s.getRow() + rOff, s.getCol() + cOff);
    }


    /**
     * Prints the current board to a more viewable format of a picture.
     * <p>
     * For testing uses.
     */
    public static void showBoard() {
        try {
            File f = new File("resources/board.png");
            BufferedImage b = ImageIO.read(f);
            Graphics g = b.getGraphics();
            BufferedImage b2;
            Square s;

            for(int i = 0; i < 8; i++) {
                for(int j = 0; j<8; j++) {
                    s = board[i][j];
                    if(s.getOccupant()!=null) {
                        b2 = ImageIO.read(new File("resources/"+s.getOccupant().toString()+".png"));
                        g.drawImage(b2, j*45, i*45, null);
                    }
                   if(s.getFlag()) {
                       g.setColor(Color.RED);
                       g.drawOval(j*45, i*45, 45, 45);
                   }
                   g.setColor(Color.RED);
                   g.drawString(s.getFileRank(), j*45, i*45 + 40);
                }
            }

            g.dispose();
            File f2 = new File("resources/board_state.png");
            ImageIO.write(b, "png", f2);
            Desktop.getDesktop().open(f2);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    /**
     * Tests to see if a given team is in "Check"
     *
     * @param color     team to test for
     * @return          {@code true} if in "Check". {@code false} otherwise.
     */
    public static boolean isInCheck(Piece.Color color) {
        Square kingLocation;
        List<AttackRoutes> enemyRoutes;

        generatePossibleMovesForColor(color.flipColor());
        if(color.equals(Piece.Color.BLACK)) {
            kingLocation = blackKingSpot;
            enemyRoutes = whitePossibleMoves;
        }
        else {
            kingLocation = whiteKingSpot;
            enemyRoutes = blackPossibleMoves;
        }

        for(AttackRoutes enemyRoute : enemyRoutes) {
            if(enemyRoute.containsPossibleSquare(kingLocation)) {
                return true;
            }
        }

        return false;
    }


    /**
     * Tests to see if a given team is in "Checkmate"
     *
     * @param color     team to test for
     * @return          {@code true} if in "Checkmate". {@code false} otherwise.
     */
    public static boolean isInCheckMate(Piece.Color color) {
        commitState();

        List<AttackRoutes> attackingTeamRoutes;
        List<AttackRoutes> defendingTeamRoutes;
        Square kingLocation;

        generatePossibleMovesForColor(color.flipColor());
        generatePossibleMovesForColor(color);

        if(color == Piece.Color.WHITE) {
            defendingTeamRoutes = whitePossibleMoves;
            attackingTeamRoutes = blackPossibleMoves;
            kingLocation = whiteKingSpot;
        } else {
            defendingTeamRoutes = blackPossibleMoves;
            attackingTeamRoutes = whitePossibleMoves;
            kingLocation = blackKingSpot;
        }

        AttackRoutes checkRoutes = AttackRoutes.getCheckRoutes(attackingTeamRoutes, kingLocation);
        AttackRoutes defendingRoutes = new AttackRoutes();

        for(AttackRoutes ar : defendingTeamRoutes) {
            defendingRoutes = AttackRoutes.merge(defendingRoutes, ar);
        }

        for(Path checkPath : checkRoutes.getPaths()) {
            for(Path defensePath : defendingRoutes.getPaths()) {
                Path defensivePath;

                // Block the check
                if((defensivePath = defensePath.intersect(checkPath)).size() != 0) {

                    // Test to see if we can get out of check with this move
                    try {
                        movePiece(defensivePath.get(0), defensivePath.get(1), defensivePath, false);
                        revertState();
                    } catch (InvalidMoveException e) {
                        // We've tested an invalid move which means it resulted in check
                        revertState();
                        continue;
                    }
                    return false;
                }
            }
        }
        revertState();
        return true;
    }


    /**
     * Tests to see if a given team is in a "Stalemate"
     *
     * @param color     team to test for
     * @return          {@code true} if in "Stalemat". {@code false} otherwise.
     */
    public static boolean isInStaleMate(Piece.Color color) {
        commitState();

        List<AttackRoutes> movingPlayersPossibleMoves;
        AttackRoutes possibleRoutes = new AttackRoutes();

        generatePossibleMovesForColor(color);

        if(color == Piece.Color.WHITE) {
            movingPlayersPossibleMoves = whitePossibleMoves;
        } else {
            movingPlayersPossibleMoves = blackPossibleMoves;
        }

        for(AttackRoutes ar : movingPlayersPossibleMoves) {
            possibleRoutes = AttackRoutes.merge(possibleRoutes, ar);
        }

        for(Path path : possibleRoutes.getPaths()) {
            for(Path subPath : path.getAllPaths()) {

                // Test to see if a move is valid
                try {
                    movePiece(subPath.get(0), subPath.get(1), subPath, false);
                    revertState();
                } catch (InvalidMoveException e) {
                    // We've tested an invalid move which means it resulted in check
                    revertState();
                    continue;
                }

                return false;
            }
        }
        return true;
    }
}
