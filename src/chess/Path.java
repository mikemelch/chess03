package chess;

import java.util.ArrayList;
import java.util.List;

import chess.Chess.InvalidMoveException;
import pieces.Queen;


/**
 * Path is a structure for storing all the Squares along which a Piece can move,
 * including the start square. Path also flags special types of moves and processes
 * the side effects of these moves (ex: Castling.) Path is the building block of
 * AttackRoutes.
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 * @see Square
 */
public class Path extends ArrayList<Square> {

    /**
     * Default serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Possible types for a Path
     */
    public enum PathType {DEFAULT, CASTLE, ENPASSANT, PAWN_DOUBLE, PAWN_PROMOTION};

    /**
     * Stores path type for a Path
     */
    private PathType pathType;

    /**
     * Stores Squares for a Path if it has side effects
     * <p>
     * Example: Rook moving in a Castle or removing a Pawn in an En Passant
     *
     * @see Square
     */
    private Square[] pathSideEffects = new Square[2];


    /**
     * Creates a new Path with DEFAULT PathType
     */
    public Path() {
        pathType = PathType.DEFAULT;
    }


    /**
     * Creates a new Path with a given type
     *
     * @param type  type to assign to Path
     */
    public Path(PathType type) {
        this.pathType = type;
    }


    /**
     * Creates a new Path with a given type and one side effect Square.
     *
     * @param type          type to assign to Path
     * @param sideEffect    side effect Square
     * @see Square
     */
    public Path(PathType type, Square sideEffect) {
        this(type);
        this.pathSideEffects[0] = sideEffect;
    }


    /**
     * Creates a new Path with a given type and two side effect Squares.
     * <p>
     * (Note: usually used for creating a Castle path with the side effect
     * squares being the Rook's starting and ending positions.)
     *
     * @param type          type to assign to Path
     * @param sideEffect1   first side effect Square
     * @param sideEffect2   second side effect Square
     * @see Square
     */
    public Path(PathType type, Square sideEffect1, Square sideEffect2) {
        this(type, sideEffect1);
        this.pathSideEffects[1] = sideEffect2;
    }


    /**
     * Processes a Path's side effects. This actual side effect is different for
     * each PathType.
     * <p>
     * <code>DEFAULT</code>         no side effect
     * <br>
     * <code>ENPASSANT</code>       removes the Pawn of the non-moving player
     * <br>
     * <code>CASTLE</code>          moves the Rook to the appropriate location
     * <br>
     * <code>PAWN_PROMOTION</code>  promotes the Pawn to a Queen
     *
     * @throws InvalidMoveException if a Castle results in "Check" or is invalid
     */
    public void processSideEffects() throws InvalidMoveException {
        if(this.pathType == PathType.CASTLE) {
            Board.movePiece(pathSideEffects[0], pathSideEffects[1], null);
        }
        else if(this.pathType == PathType.ENPASSANT) {
            pathSideEffects[0].removeOccupant();
        }
        else if(this.pathType == PathType.PAWN_PROMOTION) {
            pathSideEffects[0].setOccupant(new Queen(pathSideEffects[0].getOccupantColor()));
        }
    }


    /**
     * Returns all possible moves within a Path.
     * <p>
     * Example: if a Bishop can move from c1 -> f4, this method will return
     * [[c1, d2], [c1, e3], [c1, f4]]
     *
     * @return  all possible moves within a Path
     */
    public List<Path> getAllPaths() {
        List<Path> allPaths = new ArrayList<Path>();
        if(this.size() == 2) {
            allPaths.add(this);
            return allPaths;
        }

        for(int i = 1; i < this.size(); i++) {
            Path p = new Path();
            p.add(this.get(0));
            p.add(this.get(i));
            allPaths.add(p);
        }
        return allPaths;
    }


    /**
     * Tests to see whether two Paths intersect or not.
     *
     * @param path  Path to check for intersection with
     * @return      the Path with which the paths intersect
     */
    public Path intersect(Path path) {
        Path p = path.copy();
        p.clear();
        for(Square sq : this) {
            if(path.contains(sq)) {
                p.add(this.get(0));
                p.add(sq);

                // Occurs when processing the King's moves from within Check
                if(this.get(0).equals(sq)) {
                    return this;
                }
                return p;
            }
        }
        return p;
    }


    /**
     * Copies the calling Path.
     *
     * @return  copy of the calling Path
     */
    public Path copy() {
        Path newPath = new Path(this.getPathType(), this.pathSideEffects[0], this.pathSideEffects[1]);
        for(Square square : this) {
            newPath.add(square);
        }
        return newPath;
    }


    /**
     * {@link Path#pathType}
     */
    public void setPathType(PathType type) {
        this.pathType = type;
    }


    /**
     * {@link Path#pathType}
     */
    public PathType getPathType() {
        return this.pathType;
    }


    /**
     * {@link Path#pathSideEffects}
     */
    public void setSideEffects(Square sq1, Square sq2) {
        this.pathSideEffects[0] = sq1;
        this.pathSideEffects[1] = sq2;
    }


    /**
     * Tests to see whether the calling Path has side effects
     *
     * @return  <code>true</code> if it does
     *          <br>
     *          <code>false</code> otherwise
     */
    public boolean hasSideEffects() {
        if(this.pathType != PathType.DEFAULT) {
            return true;
        } else {
            return false;
        }
    }

}
