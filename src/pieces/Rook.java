package pieces;

import java.util.List;

import chess.AttackRoutes;
import chess.Board;
import chess.Path;
import chess.Square;


/**
 * Rook is a Piece which can move horizontally and vertically.
 * Each player starts with two.
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 *
 * @see Piece
 */
public class Rook extends Piece implements TrackedPiece {

    /**
     * Piece code.
     */
    private static final String PIECE_CODE = "R";

    /**
     * Keeps track of whether the Rook has moved.
     */
    private boolean hasMoved;


    /**
     * Creates new Rook with a given color.
     *
     * @param color     color to assign
     */
    public Rook(Color color) {
        super(color);
        hasMoved = false;
    }


    /**
     * @see Piece#getPieceCode()
     */
    @Override
    public String getPieceCode() {
        return PIECE_CODE;
    }


    /**
     * @see Piece#generatePossibleMoves(Square)
     */
    @Override
    public AttackRoutes generatePossibleMoves(Square start) {
        Path possiblePath = new Path();
        AttackRoutes attackRoutes = new AttackRoutes();

        int [][] directionCombos = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        Square curr;

        for(int[] directions : directionCombos) {
            int rOff = directions[0];
            int cOff = directions[1];

            curr = Board.getSquareWithOffset(start, rOff, cOff);
            while(curr != null && curr.isEmpty()) {
                possiblePath.add(curr);
                curr = Board.getSquareWithOffset(curr, rOff, cOff);
            }
            if(curr != null && !this.isSameTeam(curr.getOccupant())) {
                possiblePath.add(curr);
            }
            if(possiblePath.size() != 0 || (possiblePath.size() == 1 && possiblePath.contains(start))) {
                attackRoutes.addPath(possiblePath);
                possiblePath.clear();
            }
        }

        // Adds start square to the beginning of each possible path
        for(List<Square> paths : attackRoutes.getPaths()) {
            paths.add(0, start);
        }
        return attackRoutes;
    }


    /**
     * @see TrackedPiece#hasMoved()
     */
    @Override
    public boolean hasMoved() {
        return this.hasMoved;
    }


    /**
     * @see TrackedPiece#setMoved(boolean)
     */
    @Override
    public void setMoved(boolean b) {
        this.hasMoved = b;
    }

}
