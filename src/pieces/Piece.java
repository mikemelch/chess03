package pieces;

import chess.AttackRoutes;
import chess.Square;


/**
 * Piece is a structure which holds all the necessary information about a particular Piece.
 * The actual Piece objects are held within the Square that they occupy.
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 *
 * @see Square
 */
public abstract class Piece {

    /**
     * Color is an enum which controls the color of a Piece.
     *
     * @author Michael Melchione
     * @author Aaron Rosenheck
     */
    public enum Color {

        /**
         * Black color.
         */
        BLACK,

        /**
         * White color.
         */
        WHITE;


        /**
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            if(this == BLACK) {
                return "Black";
            }
            else {
                return "White";
            }
        }


        /**
         * Retrieves the opposite color of the current Piece.Color instance. Note: the color
         * remains unchanged because this is a static function.
         *
         * @return  opposite color
         */
        public Color flipColor() {
            if(this == BLACK) {
                return WHITE;
            } else{
                return BLACK;
            }
        }
    };


    /**
     * Color of the piece.
     */
    private Color color;


    /**
     * Creates a new Piece with a given color.
     *
     * @param color     color to give to the Piece.
     *
     * @see Piece.Color
     */
    public Piece(Color color) {
        this.color = color;
    }


    /**
     * Retrieves the color of the Piece.
     *
     * @return  color of the Piece instance.
     *
     * @see Piece.Color
     */
    public Color getColor(){
        return color;
    }


    /**
     * Tests to see if two Pieces are on the same team (have the same color).
     *
     * @param piece     Piece to check against the Piece instance
     * @return          {@code true} if they are the same
     *                  <br>
     *                  {@code false} otherwise
     */
    public boolean isSameTeam(Piece piece) {
        if(piece == null) {
            return false;
        }
        return piece.getColor().equals(this.getColor());
    }


    /**
     * Retrieves the color code of a Piece. This is in the form of "b" for black
     * and "w" for white. This is used by the Board representation.
     *
     * @return  String representation of the Piece's color
     *
     * @see Board
     */
    public String getColorCode() {
        if(this.getColor().equals(Piece.Color.BLACK)) {
            return "b";
        }
        else {
            return "w";
        }
    }


    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getColorCode() + getPieceCode();
    }


    /**
     * Returns a newly created Piece from a Piece Code and a given color. This is
     * used for pawn promotion.
     *
     * @param code      String representation of Piece type (ex. "N" for Knight)
     * @param color     color to assign to new Piece
     * @return          newly created Piece.
     *
     * @see Piece.Color
     */
    public static Piece getPieceFromCode(String code, Piece.Color color) {
        switch(code) {
            case "N":
                return new Knight(color);
            case "R":
                return new Rook(color);
            case "B":
                return new Bishop(color);
            default:
                return new Queen(color);
        }
    }


    /**
     * Retrieves the String representation of a Piece's type.
     *
     * @return  String representation of Piece type
     */
    public abstract String getPieceCode();


    /**
     * Generates all possible moves for a Piece.
     *
     * @param start     Square which the Piece currently occupies
     * @return          all possible AttackRoutes for the Piece
     *
     * @see AttackRoutes
     * @see Square
     */
    public abstract AttackRoutes generatePossibleMoves(Square start);
}
