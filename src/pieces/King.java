package pieces;

import chess.AttackRoutes;
import chess.Board;
import chess.Path;
import chess.Square;


/**
 * King is a Piece which can move one Square in every direction.
 * Each player starts with one and the goal is to defend yours.
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 *
 * @see Piece
 */
public class King extends Piece implements TrackedPiece {

    /**
     * Piece code
     */
    private static final String PIECE_CODE = "K";

    /**
     * Keeps track of whether the King has moved.
     */
    private boolean hasMoved;


    /**
     * Creates a new King with a given color.
     *
     * @param color     color to assign
     */
    public King(Color color) {
        super(color);
        hasMoved = false;
    }


    /**
     * @see Piece#getPieceCode()
     */
    @Override
    public String getPieceCode() {
        return PIECE_CODE;
    }


    /**
     * @see Piece#generatePossibleMoves(Square)
     */
    @Override
    public AttackRoutes generatePossibleMoves(Square start) {
        Path possiblePath = new Path();
        AttackRoutes attackRoutes = new AttackRoutes();
        Square curr;

        int[] directions = {-1, 0 ,1};

        for(int rOff : directions) {
            for(int cOff : directions) {
                if(Math.abs(rOff) == 1 || Math.abs(cOff) == 1) {
                    curr = Board.getSquareWithOffset(start, rOff, cOff);
                    if(curr != null && canMoveHere(curr)) {
                        possiblePath.add(start);
                        possiblePath.add(curr);
                        attackRoutes.addPath(possiblePath);
                        possiblePath.clear();
                    }
                }
            }
        }
        AttackRoutes castle = Board.getCastlesForColor(this.getColor());
        if(castle != null) {
            AttackRoutes.merge(attackRoutes, castle);
        }
        return attackRoutes;
    }


    /**
     * Tests to see if the King can move to a given Square.
     *
     * @param s     Square to check
     * @return      {@code true} if King can move here
     *              <br>
     *              {@code false} otherwise
     */
    private boolean canMoveHere(Square s) {
        return s.isEmpty() || !this.isSameTeam(s.getOccupant());
    }


    /**
     * @see TrackedPiece#hasMoved()
     */
    @Override
    public boolean hasMoved() {
        return this.hasMoved;
    }


    /**
     * @see TrackedPiece#setMoved(boolean)
     */
    @Override
    public void setMoved(boolean b) {
        this.hasMoved = b;
    }

}
