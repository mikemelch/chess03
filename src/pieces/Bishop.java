package pieces;

import java.util.List;

import chess.AttackRoutes;
import chess.Board;
import chess.Path;
import chess.Square;


/**
 * Bishop is a Piece which can move diagonally. Each player starts with two.
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 *
 * @see Piece
 */
public class Bishop extends Piece {

    /**
     * Piece code
     */
    private static final String PIECE_CODE = "B";


    /**
     * Creates a new Bishop with a given color.
     *
     * @param color     color to assign
     */
    public Bishop(Color color) {
        super(color);
    }


    /**
     * @see Piece#generatePossibleMoves(Square)
     */
    @Override
    public AttackRoutes generatePossibleMoves(Square start) {
        Path possiblePath = new Path();
        AttackRoutes attackRoutes = new AttackRoutes();

        int[] direction = {-1, 1};
        Square currentSquare;

        for(int rowDirection : direction) {
            for(int colDirection : direction) {
                currentSquare = Board.getSquareWithOffset(
                        start, rowDirection, colDirection);

                while(currentSquare != null && currentSquare.isEmpty()) {

                    possiblePath.add(currentSquare);
                    currentSquare = Board.getSquareWithOffset(
                            currentSquare, rowDirection, colDirection);
                }
                //Still might be able to go one more square & capture
                if(currentSquare != null && !this.isSameTeam(currentSquare.getOccupant())) {
                    // This is a potential capture
                    possiblePath.add(currentSquare);
                }
                if(possiblePath.size() != 0) {
                    attackRoutes.addPath(possiblePath);
                    possiblePath.clear();
                }
            }
        }
        // Adds start square to the beginning of each possible path
        for(List<Square> paths : attackRoutes.getPaths()) {
            paths.add(0, start);
        }
        return attackRoutes;
    }


    /**
     * @see Piece#getPieceCode()
     */
    @Override
    public String getPieceCode() {
        return PIECE_CODE;
    }

}

