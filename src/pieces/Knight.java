package pieces;

import chess.AttackRoutes;

import chess.Board;
import chess.Path;
import chess.Square;


/**
 * Knight is a Piece which can move in "L" shaped patterns. Each player starts with two.
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 *
 * @see Piece
 */
public class Knight extends Piece {

    /**
     * Piece code.
     */
    private static final String PIECE_CODE = "N";


    /**
     * Creates a new Knight with a given color.
     *
     * @param color     color to assign
     */
    public Knight(Color color) {
        super(color);
    }


    /**
     * @see Piece#generatePossibleMoves(Square)
     */
    @Override
    public AttackRoutes generatePossibleMoves(Square start) {
        Path possiblePath = new Path();
        AttackRoutes attackRoutes = new AttackRoutes();

        int[] offSets = {-1, -2, 1, 2};

        for(int rowOff : offSets) {
            for(int colOff : offSets) {
                if(Math.abs(rowOff) != Math.abs(colOff)) {
                    Square destination = Board.getSquareWithOffset(start, rowOff, colOff);
                    if(spaceAvailable(destination)) {
                        possiblePath.add(start);
                        possiblePath.add(destination);
                        attackRoutes.addPath(possiblePath);
                        possiblePath.clear();
                    }
                }
            }
        }

        return attackRoutes;
    }


    /**
     * Tests to see if the Knight can move to a given Square
     *
     * @param sq    Square to check
     * @return      {@code true} if Knight can move here
     *              <br>
     *              {@code false} otherwise
     */
    private boolean spaceAvailable(Square sq) {
        boolean canMoveHere = false;

        if(sq != null) {
            if(sq.isEmpty()) {
                canMoveHere = true;
            }
            else {
                Piece occupant = sq.getOccupant();
                if(!this.isSameTeam(occupant)) {
                    canMoveHere = true;
                }
            }
        }
        return canMoveHere;
    }


    /**
     * @see Piece#getPieceCode()
     */
    @Override
    public String getPieceCode() {
        return PIECE_CODE;
    }
}

