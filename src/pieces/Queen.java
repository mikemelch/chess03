package pieces;

import chess.AttackRoutes;
import chess.Square;


/**
 * Queen is a Piece which can move diagonally, horizontally, and vertically.
 * Each player starts with one.
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 *
 * @see Piece
 */
public class Queen extends Piece {

    /**
     * Piece code.
     */
    private static final String PIECE_CODE = "Q";


    /**
     * Creates new Queen with a given color.
     *
     * @param color     color to assign
     */
    public Queen(Color color) {
        super(color);
    }


    /**
     * @see Piece#getPieceCode()
     */
    @Override
    public String getPieceCode() {
        return PIECE_CODE;
    }


    /**
     * @see Piece#generatePossibleMoves(Square)
     */
    @Override
    public AttackRoutes generatePossibleMoves(Square start) {
        AttackRoutes rookAttackRoutes = new AttackRoutes();
        AttackRoutes bishopAttackRoutes = new AttackRoutes();

        // A Queen is basically a Rook and Bishop combined into one piece
        rookAttackRoutes = new Rook(this.getColor()).generatePossibleMoves(start);
        bishopAttackRoutes = new Bishop(this.getColor()).generatePossibleMoves(start);
        return AttackRoutes.merge(rookAttackRoutes, bishopAttackRoutes);
    }

}
