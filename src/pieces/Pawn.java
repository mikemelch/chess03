package pieces;

import chess.AttackRoutes;
import chess.Board;
import chess.Path;
import chess.Square;
import chess.Path.PathType;


/**
 * Pawn is a Piece which can move one Square forward (or two if it hasn't yet moved.
 * Each player starts with eight.
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 *
 * @see Piece
 */
public class Pawn extends Piece implements TrackedPiece {

    /**
     * Piece code.
     */
    private static String PIECE_CODE = "p";

    /**
     * Keeps track of whether the Pawn has moved.
     */
    private boolean hasMoved;

    /**
     * Direction which the Pawn can move.
     */
    private int direction;


    /**
     * Creates a new Pawn with a given color.
     *
     * @param color     color to assign
     */
    public Pawn(Color color) {
        super(color);
        hasMoved = false;

        //Black pawns can only go to rows with greater indexes.
        if(color.equals(Piece.Color.BLACK)) {
            direction = 1;
        }
        else {
            direction = -1;
        }
    }


    /**
     * @see Piece#generatePossibleMoves(Square)
     */
    @Override
    public AttackRoutes generatePossibleMoves(Square start) {
        AttackRoutes attackRoutes = new AttackRoutes();
        Path possiblePath = new Path();

        Square destination;
        int colOffset;
        int rowOffset;
        int newRow;
        int newCol;

        //Check forward one.
        colOffset = 0;
        rowOffset = direction;
        newRow = start.getRow() + rowOffset;
        newCol = start.getCol() + colOffset;
        destination = Board.getSquare(newRow, newCol);

        if(destination != null && destination.isEmpty()) { // If destination is on board
            if(destination.getRow() == 0 || destination.getRow() == 7) {
                possiblePath.setPathType(PathType.PAWN_PROMOTION);
                possiblePath.setSideEffects(destination, null);
            }
            possiblePath.add(start);
            possiblePath.add(destination);
            attackRoutes.addPath(possiblePath);
            possiblePath.clear();

            if(!hasMoved) { // Check forward two if pawn hasn't moved yet
                destination = Board.getSquare(newRow + direction, newCol);
                if(destination != null && destination.isEmpty()) {
                    possiblePath.add(start);
                    possiblePath.add(destination);
                    possiblePath.setPathType(PathType.PAWN_DOUBLE);
                    attackRoutes.addPath(possiblePath);
                    possiblePath.clear();
                }
            }

        }

        colOffset = -1; // Check diagonal left first
        rowOffset = direction;
        newCol = start.getCol() + colOffset;
        newRow = start.getRow() + rowOffset;


        for(int i = 0; i < 2; i++) {
            destination = Board.getSquare(newRow, newCol);
            if(destination != null) { // Square must be on board
                Piece occupant = destination.getOccupant();
                if(!destination.isEmpty() && !this.isSameTeam(occupant)) {
                    possiblePath.add(start);
                    possiblePath.add(destination); // This is a capture
                    attackRoutes.addPath(possiblePath);
                    possiblePath.clear();
                }
                else if(destination.isEmpty() && Board.enpassantSquare != null && Board.enpassantSquare.equals(destination)) {
                    possiblePath = new Path(PathType.ENPASSANT, Board.getSquare(start.getRow(), destination.getCol()));
                    possiblePath.add(start);
                    possiblePath.add(destination);
                    attackRoutes.addPath(possiblePath);
                    possiblePath.clear();
                }
            }
            colOffset = 1; // Change it to diagonal right
            newCol = start.getCol() + colOffset;
        }
        return attackRoutes;
    }


    /**
     * @see Piece#getPieceCode()
     */
    @Override
    public String getPieceCode() {
        return PIECE_CODE;
    }


    /**
     * @see TrackedPiece#setMoved(boolean)
     */
    @Override
    public void setMoved(boolean b) {
        this.hasMoved = b;
    }


    /**
     * @see TrackedPiece#hasMoved()
     */
    @Override
    public boolean hasMoved() {
        return this.hasMoved;
    }

}

