package pieces;


/**
 * TrackedPiece is an interface defined to be implemented by Pieces which
 * have particular moves that need to know whether the Piece has moved yet
 * or not.
 * <p>
 * Any piece which implemented this must also have a boolean instance variable
 * named "hasMoved".
 *
 * @author Michael Melchione
 * @author Aaron Rosenheck
 *
 * @see Piece
 */
public interface TrackedPiece {

    /**
     * Set whether the Piece has been moved.
     *
     * @param b     boolean value to assign
     */
    public void setMoved(boolean b);


    /**
     * Retrieve whether or not the Piece has moved yet.
     *
     * @return      {@code true} if the Piece has moved
     *              <br>
     *              {@code false} otherwise
     */
    public boolean hasMoved();
}
